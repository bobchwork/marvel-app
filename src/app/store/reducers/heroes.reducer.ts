import { createReducer, createSelector, on } from '@ngrx/store'
import * as dashboardActions from '../actions/dahsboard.actions'
import { HeroesState } from '../../shared/interfaces/states'

export const initialState: HeroesState = {
  list: [],
  pageSize: 25
}

export const heroesReducer = createReducer(
  initialState,
  on(dashboardActions.heroesLoaded, (state, payload) => ({
    ...state,
    list: [...state.list, ...payload.list],
    lastId: payload.lastId
  })),
  on(dashboardActions.createHero, (state, payload) => ({ ...state, list: [payload, ...state.list] })),
  on(dashboardActions.deleteHero, (state, payload) => ({
    ...state,
    list: [...state.list.filter(hero => hero.guid !== payload.guid)]
  })),
  on(dashboardActions.updateHero, (state, payload) => {
    const newList = state.list.map(hero => {
      if (hero.guid === payload.guid) {
        return { ...payload }
      }
      return hero
    })
    return { ...state, list: newList }
  })
)
