import { AppState, HeroesState } from '../../shared/interfaces/states'
import { createFeatureSelector, createSelector } from '@ngrx/store'

export const heroesFeatureKey = 'heroes'
export const heroesFeature = createFeatureSelector<HeroesState>(heroesFeatureKey)

export const selectHeroes = (state: AppState) => createSelector(heroesFeature, (state: HeroesState) => state)

export const selectHeroesState = createSelector(heroesFeature, (state: HeroesState) => {
  return state
})

export const selectHeroesList = createSelector(heroesFeature, (state: HeroesState) => state.list)
