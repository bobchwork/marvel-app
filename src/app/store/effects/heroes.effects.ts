import { Injectable } from '@angular/core'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { map, mergeMap, catchError } from 'rxjs/operators'
import { HeroesService } from '../../shared/services/heroes.service'
import { DASHBOARD } from '../types'
import * as dasboardActions from '../actions/dahsboard.actions'
import { Store } from '@ngrx/store'
import { EMPTY } from 'rxjs'
import { heroesLoaded } from '../actions/dahsboard.actions'

@Injectable()
export class HeroesEffects {
  constructor(private actions$: Actions, private heroesService: HeroesService, private store: Store) {}

  getHeroes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DASHBOARD.LOAD_HEROES),
      mergeMap(() =>
        this.heroesService.getHeroes().pipe(
          map(heroes => {
            const heroesWithGuid = this.heroesService.assignGuidToHeroes(heroes)
            // const lastItem = heroesWithGuid[heroesWithGuid.length - 1]
            return heroesLoaded({ list: heroesWithGuid, lastId: 'lastItem.guid' })
          }),
          catchError(() => EMPTY)
        )
      )
    )
  })
}
