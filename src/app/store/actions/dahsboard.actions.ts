import { createAction, props } from '@ngrx/store'
import { IHero } from '../../shared/interfaces/IHero'

import { DASHBOARD } from '../types'

export const getHeroes = createAction(DASHBOARD.LOAD_HEROES, props<{ offset?: number; guid?: string }>())

export const getHeroesError = createAction(DASHBOARD.LOAD_HEROES_ERROR)

export const heroesLoaded = createAction(DASHBOARD.HEROES_LOADED, props<{ list: IHero[]; offset?: number; lastId?: string }>())

export const deleteHero = createAction(DASHBOARD.DELETE_HERO, props<IHero>())

export const heroDeleted = createAction(DASHBOARD.HERO_DELETED)

export const heroDeleteError = createAction(DASHBOARD.HERO_DELETED_ERROR)

export const updateHero = createAction(DASHBOARD.UPDATE_HERO, props<IHero>())

export const updateHeroError = createAction(DASHBOARD.UPDATE_HERO_ERROR)
export const heroUpdated = createAction(DASHBOARD.HERO_UPDATED)

export const heroUpdateError = createAction(DASHBOARD.UPDATE_HERO_ERROR)

export const createHero = createAction(DASHBOARD.CREATE_HERO, props<IHero>())
export const heroCreated = createAction(DASHBOARD.HERO_CREATED, props<IHero>())
export const createHeroError = createAction(DASHBOARD.CREATE_HERO_ERROR)
