import { NgModule } from '@angular/core'
import { StoreModule } from '@ngrx/store'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { SharedModule } from './shared/shared.module'
import { DashboardModule } from './features/dashboard/dashboard.module'
import { heroesReducer } from './store/reducers/heroes.reducer'
import { EffectsModule } from '@ngrx/effects'
import { HeroesEffects } from './store/effects/heroes.effects'
import { heroesFeatureKey } from './store/selectors/heroes'

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    DashboardModule,
    SharedModule,
    StoreModule.forRoot({ heroes: heroesReducer }),
    StoreModule.forFeature(heroesFeatureKey, heroesReducer),
    EffectsModule.forRoot([HeroesEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 20
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
