import { Component, OnInit, ViewChild } from '@angular/core'
import { createHero, deleteHero, getHeroes, updateHero } from '../../store/actions/dahsboard.actions'
import { Hero } from '../../shared/classes/Hero'
import { HeroFormModalComponent } from './hero-form-modal/hero-form-modal.component'
import { MatDialog } from '@angular/material/dialog'
import { HeroesService } from '../../shared/services/heroes.service'
import { takeUntil } from 'rxjs/operators'
import { Destroy } from '../../shared/classes/Destroy'
import { Store } from '@ngrx/store'
import { AppState, HeroesState } from '../../shared/interfaces/states'
import { selectHeroesState } from '../../store/selectors/heroes'
import { SearchComponent } from '../../shared/components/search/search.component'
import { ConfirmationDialogComponent } from '../../shared/components/confirmation-dialog/confirmation-dialog.component'
import { MatSnackBar } from '@angular/material/snack-bar'
import { SNACKBAR_MESSAGES, SNACKBAR_TYPES } from '../../shared/constants'

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  viewProviders: [Destroy]
})
export class DashboardComponent implements OnInit {
  public heroes: Hero[] = []
  public displayHeroes = this.heroes
  @ViewChild('search') private searchComponent?: SearchComponent
  public pageSize?: number

  constructor(
    public dialog: MatDialog,
    private heroesService: HeroesService,
    private readonly destroy$: Destroy,
    private store$: Store<AppState>,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.store$.dispatch(getHeroes({}))
    this.store$
      .select(selectHeroesState)
      .pipe(takeUntil(this.destroy$))
      .subscribe((heroes: HeroesState) => {
        this.pageSize = heroes.pageSize
        if (heroes.list?.length < 1) {
          this.store$.dispatch(getHeroes({}))
        } else {
          this.heroes = heroes.list
          this.displayHeroes = this.heroes
        }
      })
  }

  public filterNamesOnTable(filteredNames: string[]) {
    if (filteredNames.length > 0) {
      this.displayHeroes = this.heroes.filter(hero => filteredNames.includes(hero.nameLabel))
    } else {
      this.resetFilter()
    }
  }

  public resetFilter(displaySnackBar = true) {
    this.displayHeroes = this.heroes
    this.searchComponent?.clearFilterData()
    if (displaySnackBar) {
      this.showSnackBar(SNACKBAR_TYPES.FILTER)
    }
  }

  public deleteHero(hero: Hero) {
    const dialog = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        heroGuid: hero.guid,
        confirmationMessage: 'Are you sure you want to delete this hero?'
      }
    })
    dialog.afterClosed().subscribe(dialogData => {
      if (dialogData.value) {
        this.store$.dispatch(deleteHero(hero))
        this.showSnackBar(SNACKBAR_TYPES.DELETE)
      }
    })
  }

  public createHero() {
    const dialogRef = this.dialog.open(HeroFormModalComponent, {
      data: {}
    })
    dialogRef.afterClosed().subscribe(hero => {
      if (hero) {
        const newHero = this.heroesService.createHeroWithGuid(hero)
        this.store$.dispatch(createHero({ ...newHero }))
        this.showSnackBar(SNACKBAR_TYPES.CREATE)

        this.resetFilter(false)
      }
    })
  }

  public updateHero(hero: Hero) {
    const dialogRef = this.dialog.open(HeroFormModalComponent, {
      data: hero
    })
    dialogRef.afterClosed().subscribe(hero => {
      if (hero) {
        this.store$.dispatch(updateHero({ ...hero }))
        this.showSnackBar(SNACKBAR_TYPES.UPDATE)
      }
    })
  }

  // TODO: ADD TO A SEPARATE SERVICE
  public showSnackBar(type: {}) {
    switch (type) {
      case SNACKBAR_TYPES.FILTER:
        this._snackBar.open(SNACKBAR_MESSAGES.FILTER_RESET, 'Filter reset', {
          duration: 1000
        })
        break
      case SNACKBAR_TYPES.UPDATE:
        this._snackBar.open(SNACKBAR_MESSAGES.ITEM_UPDATED, 'Update hero', {
          duration: 1000
        })
        break
      case SNACKBAR_TYPES.DELETE:
        this._snackBar.open(SNACKBAR_MESSAGES.ITEM_DELETED, 'Delete hero', {
          duration: 1000
        })
        break
      case SNACKBAR_TYPES.ERROR:
        this._snackBar.open(SNACKBAR_MESSAGES.ERROR, 'Error', {
          duration: 1000
        })
        break

      case SNACKBAR_TYPES.CREATE:
        this._snackBar.open(SNACKBAR_MESSAGES.ITEM_CREATED, 'Create hero', {
          duration: 1000
        })
    }
  }
}
