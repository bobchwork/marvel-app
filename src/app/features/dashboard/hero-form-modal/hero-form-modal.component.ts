import { AfterViewChecked, AfterViewInit, Component, Inject, OnInit, ViewChild } from '@angular/core'
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog'
import { Hero } from '../../../shared/classes/Hero'
import { IHero } from '../../../shared/interfaces/IHero'
import { HeroFormComponent } from './hero-form/hero-form.component'
import { Destroy } from '../../../shared/classes/Destroy'
import { takeUntil } from 'rxjs/operators'

@Component({
  selector: 'hero-form-modal',
  templateUrl: './hero-form-modal.component.html',
  styleUrls: ['./hero-form-modal.component.scss'],
  viewProviders: [Destroy]
})
export class HeroFormModalComponent implements OnInit, AfterViewInit {
  @ViewChild(HeroFormComponent) private heroForm?: HeroFormComponent
  public heroFormData: any = {}
  constructor(@Inject(MAT_DIALOG_DATA) public data: IHero, private readonly destroy$: Destroy) {}

  ngOnInit() {}
  ngAfterViewInit() {
    this.heroForm?.heroGroupForm.valueChanges.pipe(takeUntil(this.destroy$)).subscribe(heroData => {
      this.data = heroData
    })
  }
}
