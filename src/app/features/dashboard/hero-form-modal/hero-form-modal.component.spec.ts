import { ComponentFixture, TestBed } from '@angular/core/testing'
import { HeroFormModalComponent } from './hero-form-modal.component'

describe('HeroFormModalComponent', () => {
  let component: HeroFormModalComponent
  let fixture: ComponentFixture<HeroFormModalComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeroFormModalComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroFormModalComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
