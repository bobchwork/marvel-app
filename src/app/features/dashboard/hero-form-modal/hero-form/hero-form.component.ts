import { AfterContentInit, AfterViewInit, Component, Input, OnInit } from '@angular/core'
import { Hero } from '../../../../shared/classes/Hero'
import { FormBuilder } from '@angular/forms'

@Component({
  selector: 'hero-form',
  templateUrl: './hero-form.component.html',
  styleUrls: ['./hero-form.component.scss']
})
export class HeroFormComponent implements AfterContentInit {
  @Input() data?: Hero

  public heroGroupForm = this.fb.group({
    nameLabel: [''],
    occupationLabel: [''],
    citizenshipLabel: [''],
    creatorLabel: [''],
    genderLabel: [''],
    skillsLabel: [''],
    memberOfLabel: [''],
    guid: ['']
  })

  constructor(private fb: FormBuilder) {}

  ngAfterContentInit(): void {
    if (this.data) {
      this.heroGroupForm.setValue(this.data)
    }
  }
}
