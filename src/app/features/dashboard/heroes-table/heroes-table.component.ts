import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core'
import { Hero } from '../../../shared/classes/Hero'
import { HEROES_TABLE_COLUMNS } from '../../../shared/constants'
import { MatTableDataSource } from '@angular/material/table'
import { MatSort } from '@angular/material/sort'
import { Destroy } from '../../../shared/classes/Destroy'
import { MatDialog } from '@angular/material/dialog'
import { HeroInfoModalComponent } from '../hero-info-modal/hero-info-modal.component'
import { MatPaginator } from '@angular/material/paginator'
import { HeroChartModalComponent } from '../hero-chart-modal/hero-chart-modal.component'

@Component({
  selector: 'heroes-table',
  templateUrl: './heroes-table.component.html',
  styleUrls: ['./heroes-table.component.scss'],
  viewProviders: [Destroy]
})
export class HeroesTableComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() heroes: Hero[] = []
  @Input() isActionColumnVisible: boolean = true
  @Input() customColumns: string[] = []
  @Input() pageSize?: number
  @Output() deleteHero: EventEmitter<Hero> = new EventEmitter<Hero>()
  @Output() openUpdateModal: EventEmitter<Hero> = new EventEmitter<Hero>()
  @ViewChild(MatPaginator) paginator: any

  public tableColumns = HEROES_TABLE_COLUMNS

  public columns: string[] = [
    HEROES_TABLE_COLUMNS.NAME,
    HEROES_TABLE_COLUMNS.GENDER,
    HEROES_TABLE_COLUMNS.SKILL,
    HEROES_TABLE_COLUMNS.OCCUPATION,
    HEROES_TABLE_COLUMNS.CITIZENSHIP,
    HEROES_TABLE_COLUMNS.MEMBER_OF,
    HEROES_TABLE_COLUMNS.CREATOR
  ]

  public dataSource = new MatTableDataSource(this.heroes)
  @ViewChild(MatSort) sort?: MatSort
  public pagination: number = 10

  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {
    if (this.customColumns.length > 0) {
      this.columns = this.customColumns
    }
    if (this.isActionColumnVisible) {
      this.columns.push(HEROES_TABLE_COLUMNS.ACTIONS)
    }
    this.pagination = this.pageSize || this.pagination
    this.dataSource = new MatTableDataSource(this.heroes)
  }

  ngAfterViewInit(): void {
    if (this.dataSource && this.sort) {
      this.dataSource.sort = this.sort
    }
    if (this.paginator) {
      this.dataSource.paginator = this.paginator
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    this.dataSource.data = this.heroes
  }

  public delete(hero: Hero) {
    if (this.deleteHero) {
      this.deleteHero?.emit(hero)
    }
  }

  public startUpdateAction(hero: Hero) {
    this.openUpdateModal?.emit(hero)
  }

  public showHeroData(hero: Hero) {
    this.dialog.open(HeroInfoModalComponent, {
      data: { hero },
      width: '65%'
    })
  }

  public openChartModal(label: string) {
    let labelName = label.replace('Label', '').toLocaleLowerCase()
    this.dialog.open(HeroChartModalComponent, {
      data: {
        chartData: this.getLabelValues(label),
        label: labelName.charAt(0).toUpperCase() + labelName.slice(1)
      },
      width: '100%',
      height: '90%'
    })
  }

  public getLabelValues(categoryLabel: string): string[] {
    return this.heroes.map(item => {
      // @ts-ignore
      return item[categoryLabel]
    })
  }
}
