import { ComponentFixture, TestBed } from '@angular/core/testing'

import { HeroInfoModalComponent } from './hero-info-modal.component'

describe('HeroInfoModalComponent', () => {
  let component: HeroInfoModalComponent
  let fixture: ComponentFixture<HeroInfoModalComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeroInfoModalComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroInfoModalComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
