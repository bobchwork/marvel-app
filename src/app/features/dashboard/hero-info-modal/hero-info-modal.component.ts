import { Component, Inject, OnInit } from '@angular/core'
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog'
import { Hero } from '../../../shared/classes/Hero'

@Component({
  selector: 'app-hero-info-modal',
  templateUrl: './hero-info-modal.component.html',
  styleUrls: ['./hero-info-modal.component.scss']
})
export class HeroInfoModalComponent implements OnInit {
  public hero: Hero
  constructor(@Inject(MAT_DIALOG_DATA) public data: { hero: Hero }) {
    this.hero = data.hero
  }

  ngOnInit(): void {}
}
