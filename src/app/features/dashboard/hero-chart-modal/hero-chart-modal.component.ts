import { Component, Inject, OnInit } from '@angular/core'
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog'
import { Hero } from '../../../shared/classes/Hero'

@Component({
  selector: 'hero-chart-modal',
  templateUrl: './hero-chart-modal.component.html',
  styleUrls: ['./hero-chart-modal.component.scss']
})
export class HeroChartModalComponent implements OnInit {
  public chartData: string[]
  public label: string
  constructor(@Inject(MAT_DIALOG_DATA) public data: { chartData: string[]; label: string }) {
    this.chartData = data.chartData
    this.label = data.label
  }

  ngOnInit(): void {}
}
