import { ComponentFixture, TestBed } from '@angular/core/testing'

import { HeroChartModalComponent } from './hero-chart-modal.component'

describe('HeroChartModalComponent', () => {
  let component: HeroChartModalComponent
  let fixture: ComponentFixture<HeroChartModalComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeroChartModalComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroChartModalComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
