import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DashboardComponent } from './dashboard.component'
import { SharedModule } from '../../shared/shared.module'
import { HeroesTableComponent } from './heroes-table/heroes-table.component'
import { HeroFormModalComponent } from './hero-form-modal/hero-form-modal.component'
import { HeroInfoModalComponent } from './hero-info-modal/hero-info-modal.component'
import { HeroFormComponent } from './hero-form-modal/hero-form/hero-form.component'
import { HeroChartModalComponent } from './hero-chart-modal/hero-chart-modal.component'

const entryComponents = [
  HeroInfoModalComponent,
  HeroChartModalComponent,
  HeroFormComponent,
  HeroFormModalComponent,
  DashboardComponent,
  HeroesTableComponent
]

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [...entryComponents],
  exports: [...entryComponents]
})
export class DashboardModule {}
