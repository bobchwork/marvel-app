import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core'
import * as d3 from 'd3'
import { Destroy } from '../../classes/Destroy'

@Component({
  selector: 'bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss'],
  viewProviders: [Destroy]
})
export class BarChartComponent implements OnInit, AfterViewInit {
  @ViewChild('chart') private chartContainer?: ElementRef
  @Input() public barData: any[] = []
  private margin: any = { top: 20, bottom: 20, left: 20, right: 20 }
  private chart: any
  private width: number = 1
  private height: number = 1
  private xScale: any
  private yScale: any
  private colors: any
  private xAxis: any
  private yAxis: any
  private d3 = d3
  private mappedData: any = []

  constructor(private readonly destroy$: Destroy) {}

  ngOnInit() {
    this.generateMappedData()
  }

  ngAfterViewInit() {
    this.createChart()
    if (this.mappedData.length > 0) {
      this.updateChart()
    }
  }

  private generateMappedData() {
    if (this.barData) {
      for (const [key, value] of Object.entries(this.barData)) {
        this.mappedData.push([value.label, value.value])
      }
    }
  }

  createChart() {
    // @ts-ignore
    let element = this.chartContainer.nativeElement
    this.width = element.offsetWidth - this.margin.left - this.margin.right
    this.height = element.offsetHeight - this.margin.top - this.margin.bottom
    let svg = this.d3.select(element).append('svg').attr('width', element.offsetWidth).attr('height', element.offsetHeight)

    this.chart = svg.append('g').attr('class', 'bars').attr('transform', `translate(${this.margin.left}, ${this.margin.top})`)

    let xDomain = this.mappedData.map((d: any[]) => d[0])
    let yDomain = [0, this.d3.max(this.mappedData, (d: any) => d[1])]

    this.xScale = this.d3.scaleBand().padding(0.1).domain(xDomain).rangeRound([0, this.width])
    // @ts-ignore
    this.yScale = this.d3.scaleLinear().domain(yDomain).range([this.height, 0])

    this.colors = this.d3
      .scaleLinear()
      .domain([0, this.mappedData.length])
      .range(<any[]>['blue', 'purple'])

    this.xAxis = svg
      .append('g')
      .attr('class', 'axis axis-x')
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top + this.height})`)
      .call(this.d3.axisBottom(this.xScale))
    this.yAxis = svg
      .append('g')
      .attr('class', 'axis axis-y')
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`)
      .call(this.d3.axisLeft(this.yScale))
  }

  updateChart() {
    this.xScale.domain(this.mappedData.map((d: any[]) => d[0]))
    this.yScale.domain([0, this.d3.max(this.mappedData, (d: any) => d[1])])
    this.colors.domain([0, this.mappedData.length])
    this.xAxis.transition().call(this.d3.axisBottom(this.xScale))
    this.yAxis.transition().call(this.d3.axisLeft(this.yScale))

    let update = this.chart.selectAll('.bar').data(this.mappedData)

    update.exit().remove()

    this.chart
      .selectAll('.bar')
      .transition()
      .attr('x', (d: any) => this.xScale(d[0]))
      .attr('y', (d: any) => this.yScale(d[1]))
      .attr('width', (d: any) => this.xScale.bandwidth())
      .attr('height', (d: any) => this.height - this.yScale(d[1]))
      .style('fill', (d: any, i: number) => this.colors(i))

    update
      .enter()
      .append('rect')
      .attr('class', 'bar')
      .attr('x', (d: any) => this.xScale(d[0]))
      .attr('y', (d: any) => this.yScale(0))
      .attr('width', this.xScale.bandwidth())
      .attr('height', 0)
      .style('fill', (d: any, i: number) => this.colors(i))
      .transition()
      .delay((d: any, i: number) => i * 10)
      .attr('y', (d: any) => this.yScale(d[1]))
      .attr('height', (d: any) => this.height - this.yScale(d[1]))
  }
}
