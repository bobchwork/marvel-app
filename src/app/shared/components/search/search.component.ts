import { Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, ViewChild } from '@angular/core'
import { Destroy } from '../../classes/Destroy'
import { FormControl } from '@angular/forms'
import { Observable } from 'rxjs'
import { COMMA, ENTER } from '@angular/cdk/keycodes'
import { map, startWith } from 'rxjs/operators'
import { MatChipInputEvent } from '@angular/material/chips'
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete'
import { Hero } from '../../classes/Hero'
import { Output } from '@angular/core'
import { SimpleChanges } from '@angular/core'

@Component({
  selector: 'search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  viewProviders: [Destroy]
})
export class SearchComponent implements OnInit, OnChanges {
  selectable = true
  removable = true
  separatorKeysCodes: number[] = [ENTER, COMMA]
  heroCtrl = new FormControl()
  displayHeroes?: Observable<string[]>
  heroesNames: string[] = []
  allHeroesNames: string[] = []

  @Input()
  set heroes(heroes: Hero[]) {
    this.allHeroesNames = heroes.map(heroe => heroe.nameLabel)
  }

  @Output() filterNamesFromList = new EventEmitter<string[]>()

  @ViewChild('heroInput') heroInput?: ElementRef<HTMLInputElement>

  constructor(private readonly destroy$: Destroy) {
    this.filterHeroes()
  }

  ngOnInit(): void {}

  public filterHeroes() {
    this.displayHeroes = this.heroCtrl.valueChanges.pipe(
      startWith(null),
      map((hero: string | null) => (hero ? this._filter(hero) : this.allHeroesNames.slice()))
    )
  }

  ngOnChanges(changes: SimpleChanges) {
    this.filterHeroes()
  }

  add(event: any): void {
    const value = (event.value || '').trim()

    if (value) {
      this.heroesNames.push(value)
    }
    event.chipInput!.clear()

    this.heroCtrl.setValue(null)
  }

  remove(hero: string): void {
    const index = this.heroesNames.indexOf(hero)

    if (index >= 0) {
      this.heroesNames.splice(index, 1)
    }
    this.filterNamesFromList.emit(this.heroesNames)
  }

  clearFilterData() {
    this.heroesNames = []
    this.filterNamesFromList.emit([])
  }

  selected(event: any): void {
    if (this.heroInput) {
      this.heroesNames.push(event.option.viewValue)
      this.heroInput.nativeElement.value = ''
      this.heroCtrl.setValue(null)
      this.filterNamesFromList.emit(this.heroesNames)
    }
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase()
    return this.allHeroesNames.filter(hero => hero.toLowerCase().includes(filterValue))
  }
}
