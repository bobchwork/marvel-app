import { AfterContentChecked, AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core'
import * as d3 from 'd3'
import { Guid } from 'guid-typescript'
import { BehaviorSubject } from 'rxjs'

@Component({
  selector: 'pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit, AfterViewInit {
  @Input('pieData') public pieData: any[] = []
  @Input('textColor') public textColor: string = '#ffffff'
  @Input('isPercentage') public isPercentage: boolean = false
  @Input('enablePolylines') public enablePolylines: boolean = false

  public chartId
  private svg: any
  private margin = 25
  private width = 750
  private height = 450
  public d3 = d3
  private isChartCreated = false

  private radius = Math.min(this.width, this.height) / 2 - this.margin

  private colors: any
  private data: any
  private pieDataSubject: BehaviorSubject<any[]> = new BehaviorSubject<any[]>(this.pieData)
  private pieData$ = this.pieDataSubject.asObservable()

  constructor() {
    this.chartId = Guid.create().toString()
  }

  ngOnInit(): void {
    this.generatePieData()
  }
  ngAfterViewInit() {
    if (this.data?.length > 0 && !this.isChartCreated) {
      this.createChart()
    }
  }

  private generatePieData() {
    this.data = Object.values(this.pieData)
  }

  private createChart() {
    this.createSvg()
    this.createColors()
    this.drawChart()
    this.isChartCreated = true
  }

  private createSvg(): void {
    this.svg = this.d3
      .select('figure#pie')
      .append('svg')
      .attr('viewBox', `0 0 ${this.width} ${this.height}`)
      .append('g')
      .attr('transform', 'translate(' + this.width / 2 + ',' + this.height / 2 + ')')
  }

  private createColors(data = this.data): void {
    this.colors = this.d3
      .scaleOrdinal()
      .domain(data.map((d: any) => d.value.toString()))
      .range(['#6773f1', '#32325d', '#6162b5', '#6586f6', '#8b6ced', '#1b1b1b', '#212121'])
  }

  private drawChart(data = this.data): void {
    // Compute the position of each group on the pie
    const pie = this.d3.pie<any>().value((d: any) => Number(d.value))
    const data_ready = pie(data)

    let radius = Math.min(this.width, this.height) / 2 - this.margin

    let outerArc = this.d3
      .arc()
      .innerRadius(radius * 0.9)
      .outerRadius(radius * 0.9)

    let arc = this.d3
      .arc()
      .innerRadius(radius * 0.5)
      .outerRadius(radius * 0.8)
    this.svg
      .selectAll('pieces')
      .data(data_ready)
      .enter()
      .append('path')
      .attr('d', this.d3.arc().innerRadius(0).outerRadius(this.radius))
      .attr('fill', (d: any, i: number) => (d.data.color ? d.data.color : this.colors(i)))
      .attr('stroke', '#121926')
      .style('stroke-width', '1px')
    const labelLocation = this.d3.arc().innerRadius(50).outerRadius(this.radius)
    let dy = 0
    let index = 0
    this.svg
      .selectAll('pieces')
      .data(pie(data))
      .enter()
      .append('text')
      // @ts-ignore
      .text((d: any) => {
        if (((d.endAngle - d.startAngle) / (2 * Math.PI)) * 100 > 5 || !this.enablePolylines) {
          return d.data.label + ' (' + d.data.value + (this.isPercentage ? '%' : '') + ')'
        }
      })
      .attr('transform', (d: any) => 'translate(' + labelLocation.centroid(d) + ')')
      .style('text-anchor', 'middle')
      .style('font-size', 28)
      .attr('fill', this.textColor)
    if (this.enablePolylines) {
      this.svg
        .selectAll('allLabels')
        .data(data_ready)
        .enter()
        .append('text')
        .text((d: any) => {
          if (((d.endAngle - d.startAngle) / (2 * Math.PI)) * 100 > 5) {
            return null
          } else {
            return d.data.label + ' (' + d.data.value + (this.isPercentage ? '%' : '') + ')'
          }
        })
        .style('font-size', '28px')
        .attr('dy', (d: any) => {
          if (((d.endAngle - d.startAngle) / (2 * Math.PI)) * 100 > 5) {
            return null
          } else {
            let value = 0.35
            if (index != 0) dy = dy + 1
            index++
            value = value + dy
            return value.toString() + 'em'
          }
        })
        .attr('transform', (d: any) => {
          if (((d.endAngle - d.startAngle) / (2 * Math.PI)) * 100 > 5) {
            return null
          } else {
            let pos = outerArc.centroid(d)
            let midangle = d.startAngle + (d.endAngle - d.startAngle) / 2
            pos[0] = radius * 0.99 * (midangle < Math.PI ? 1 : -1)
            return 'translate(' + pos + ')'
          }
        })
        .style('text-anchor', (d: any) => {
          let midangle = d.startAngle + (d.endAngle - d.startAngle) / 2
          return midangle < Math.PI ? 'start' : 'end'
        })
      index = 0
      let addTo = 5
      this.svg
        .selectAll('allPolylines')
        .data(data_ready)
        .enter()
        .append('polyline')
        .attr('stroke', 'black')
        .style('fill', 'none')
        .attr('stroke-width', 1)
        .attr('points', (d: any) => {
          if (((d.endAngle - d.startAngle) / (2 * Math.PI)) * 100 > 5) {
            return null
          } else {
            let posA = arc.centroid(d)
            let posB = outerArc.centroid(d)
            let posC = outerArc.centroid(d)
            let midangle = d.startAngle + (d.endAngle - d.startAngle) / 2
            posC[0] = radius * (midangle < Math.PI ? 1 : -1)
            posC[0] = posC[0] + addTo
            posC[1] = posC[1] + addTo
            addTo = addTo + 10
            return [posA, posB, posC]
          }
        })
    }
  }
}
