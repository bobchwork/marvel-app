/*
  this custom chart component takes an array and filters the coincidences
  between the elements and display the data accroding to the following
  rules:

  - If there are only 5 different values or less, use a pie chart
  - If there are more than 5 different values , use a bar chart
*/
// @ts-nocheck
import { AfterContentChecked, AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core'
import { Destroy } from '../../classes/Destroy'
import { BehaviorSubject, Observable, Subject } from 'rxjs'
import { takeUntil } from 'rxjs/operators'

@Component({
  selector: 'custom-chart',
  templateUrl: './custom-chart.component.html',
  styleUrls: ['./custom-chart.component.scss'],
  viewProviders: [Destroy]
})
export class CustomChartComponent implements OnInit, AfterContentChecked {
  @Input() chartData: string[] = []
  private _results: void[] = []
  public amount: number = 0
  public occurrences: Subject<number> = new Subject<number>(0)

  get results() {
    return this._results
  }

  set results(value) {
    this._results = value
  }

  constructor(private readonly destroy$: Destroy) {}

  ngOnInit(): void {
    this.occurrences.subscribe(value => {
      this.amount = value
    })
  }
  ngAfterContentChecked() {
    this.calculate()
  }

  private calculate(): any {
    this.chartData.map((s, i) => {
      const key: string = s.toLowerCase().replace(/[^a-z0-9]|\s+|\r?\n|\r/gim, '_')
      this._results[key] = this._results[key]
        ? { ...this._results[key], value: this._results[key].value + 1 }
        : {
            label: s,
            value: 1
          }
    })
    // const amount = Object.keys(this._results).length
    const amount = Object.keys(this._results).length
    this.occurrences.next(amount)
  }
}
