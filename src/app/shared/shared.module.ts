import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { MaterialModule } from '../material.module'
import { FlexLayoutModule } from '@angular/flex-layout'
import { FooterComponent } from './components/footer/footer.component'
import { HeaderComponent } from './components/header/header.component'
import { SearchComponent } from './components/search/search.component'
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component'
import { ReactiveFormsModule } from '@angular/forms'
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { CustomChartComponent } from './components/custom-chart/custom-chart.component'
import { PieChartComponent } from './components/pie-chart/pie-chart.component'
import { BarChartComponent } from './components/bar-chart/bar-chart.component'

const entryComponents = [
  ConfirmationDialogComponent,
  FooterComponent,
  HeaderComponent,
  SearchComponent,
  CustomChartComponent,
  PieChartComponent,
  BarChartComponent
]

@NgModule({
  imports: [CommonModule, MaterialModule, FlexLayoutModule, ReactiveFormsModule, FormsModule],
  declarations: [...entryComponents],
  exports: [CommonModule, ReactiveFormsModule, FormsModule, MaterialModule, FlexLayoutModule, ...entryComponents],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule {}
