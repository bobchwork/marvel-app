import { IChartData } from '../interfaces/IChartData'

export class ChartData implements IChartData {
  constructor(public value: number, public label: string) {}
}
