import { IHero } from '../interfaces/IHero'

export class Hero implements IHero {
  constructor(
    public nameLabel: string,
    public genderLabel: string,
    public skillsLabel: string,
    public occupationLabel: string,
    public citizenshipLabel: string,
    public memberOfLabel: string,
    public creatorLabel: string,
    public guid?: string
  ) {}
}
