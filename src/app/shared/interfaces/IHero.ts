export interface IHero {
  nameLabel: string
  genderLabel: string
  citizenshipLabel: string
  skillsLabel: string
  occupationLabel: string
  memberOfLabel: string
  creatorLabel: string
  guid?: string
}
