import { IHero } from './IHero'
import { Guid } from 'guid-typescript'

export interface HeroesState {
  list: IHero[]
  pageSize: number
  lastId?: string
}

export interface UiState {
  loading: false
}

export interface AppState {
  hero: HeroesState
  ui: UiState
}
