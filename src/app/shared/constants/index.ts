export enum HEROES_TABLE_COLUMNS {
  NAME = 'nameLabel',
  GENDER = 'genderLabel',
  CITIZENSHIP = 'citizenshipLabel',
  SKILL = 'skillsLabel',
  OCCUPATION = 'occupationLabel',
  MEMBER_OF = 'memberOfLabel',
  CREATOR = 'creatorLabel',
  ACTIONS = 'actionsLabel'
}

export enum SNACKBAR_MESSAGES {
  FILTER_RESET = 'The filter is cleared!',
  ITEM_DELETED = 'Item deleted!',
  ITEM_CREATED = 'Item created!',
  ITEM_UPDATED = 'Item updated',
  ERROR = 'Oooops! something happened'
}

export enum SNACKBAR_TYPES {
  UPDATE = 'UPDATE',
  CREATE = 'CREATE',
  FILTER = 'FILTER',
  DELETE = 'DELETE',
  ERROR = 'ERROR'
}
