import { Injectable } from '@angular/core'
import heroes from '/src/assets/files/marvel_data.json'
import { Hero } from '../classes/Hero'
import { of } from 'rxjs'
import { Guid } from 'guid-typescript'

@Injectable({
  providedIn: 'root'
})
export class HeroesService {
  constructor() {}

  public getHeroes() {
    //Emulating an http request
    const allHeroes: Hero[] = heroes
    return of(allHeroes)
  }

  public assignGuidToHeroes(heroes: Hero[]) {
    const newHeroes = heroes.map(hero => {
      hero = this.assignGuidToHero(hero)
      return hero
    })
    return [...newHeroes]
  }

  public assignGuidToHero(hero: Hero): Hero {
    const newHero = hero
    newHero.guid = Guid.create().toString()
    return { ...newHero }
  }

  public createHeroWithGuid(hero: Hero): Hero {
    return this.assignGuidToHero(hero)
  }
}
