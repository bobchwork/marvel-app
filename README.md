# MarvelApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.13.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

# Folder structure

###app:
Contains the main application
####shared:
contains the subfolders with the code that can be
shared throughout the application (shared components, constants, helpers, pipes or common services)
####store:
contains the store related subfolders (effects, reducers, selectors, actions )

##assets
All the external resources like images or fonts.

#environments
the environment variables
